import Vue from 'vue';
import App from './App.vue';
import Login from './components/Login.vue';
import VueRouter from 'vue-router';
import Home from './components/Home.vue';
import VModal from 'vue-js-modal';
import {store} from './store.js';
import TaskEdit from './components/TaskEdit.vue'
import Finder from './components/Finder.vue'

Vue.use(VueRouter);
Vue.use(VModal);

const routes = [
  {path: '/', component: Home},
  {path: '/login', component: Login},
  {path: '/task/:id', component: TaskEdit},
  {path: '/find', component: Finder}
];

const router = new VueRouter({
  mode: 'history',
  routes
});

const AppConstructor = Vue.extend(App);
new AppConstructor({
  el: '#app',
  router: router,
  store: store,
  info: null,
  computed: {
    counter: function () {
      return this.$store.state.count;
    }
  }
});
