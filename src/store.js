import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count += 1;
    },
    setId (state) {
      var currId = 0;
      axios.get('http://localhost:3000/tasks/')
        .then(response => {
          response.data.forEach((task) => {
            if (task.taskId > currId) {
              currId = task.taskId;
            }
          });
          state.count = currId + 1;
        });
    }
  },
  getters: {
    getId: state => {
      return state.count;
    }
  }
});
